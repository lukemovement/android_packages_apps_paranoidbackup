package com.paranoid.backup;

import java.io.File;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReceiver extends BroadcastReceiver 
{ 
    @Override 
    public void onReceive(Context context, Intent intent) 
    { 
    	if(Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {

	   File FirstLaunchDone = new File("/system/FirstLaunchDone");
 		if(FirstLaunchDone.exists()) {
 			
 		} else {
 		Intent i = new Intent(context, MainActivity.class);
 		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
		context.startActivity(i);
 		}
 	
 
	}
  }
}
